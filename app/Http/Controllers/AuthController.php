<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }
    public function welcome(Request $request){
        $Fname= $request['Fname'];
        $Lname= $request['Lname'];
        return view('welcome', ["Fname" => $Fname,"Lname" => $Lname]);
    }
}